package qb9.web.rest;

import qb9.GiftpinApp;
import qb9.domain.Pin;
import qb9.repository.PinRepository;
import qb9.service.PinService;
import qb9.service.dto.PinDTO;
import qb9.service.mapper.PinMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.PinStatus;
/**
 * Test class for the PinResource REST controller.
 *
 * @see PinResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GiftpinApp.class)
public class PinResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_PIN_CODE = "AAAAA";
    private static final String UPDATED_PIN_CODE = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final PinStatus DEFAULT_STATUS = PinStatus.NEW;
    private static final PinStatus UPDATED_STATUS = PinStatus.BURNED;

    @Inject
    private PinRepository pinRepository;

    @Inject
    private PinMapper pinMapper;

    @Inject
    private PinService pinService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPinMockMvc;

    private Pin pin;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PinResource pinResource = new PinResource();
        ReflectionTestUtils.setField(pinResource, "pinService", pinService);
        this.restPinMockMvc = MockMvcBuilders.standaloneSetup(pinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pin createEntity(EntityManager em) {
        Pin pin = new Pin();
        pin = new Pin();
        pin.setUid(DEFAULT_UID);
        pin.setPinCode(DEFAULT_PIN_CODE);
        pin.setCreatedAt(DEFAULT_CREATED_AT);
        pin.setUpdatedAt(DEFAULT_UPDATED_AT);
        pin.setStatus(DEFAULT_STATUS);
        return pin;
    }

    @Before
    public void initTest() {
        pin = createEntity(em);
    }

    @Test
    @Transactional
    public void createPin() throws Exception {
        int databaseSizeBeforeCreate = pinRepository.findAll().size();

        // Create the Pin
        PinDTO pinDTO = pinMapper.pinToPinDTO(pin);

        restPinMockMvc.perform(post("/api/pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pinDTO)))
                .andExpect(status().isCreated());

        // Validate the Pin in the database
        List<Pin> pins = pinRepository.findAll();
        assertThat(pins).hasSize(databaseSizeBeforeCreate + 1);
        Pin testPin = pins.get(pins.size() - 1);
        assertThat(testPin.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testPin.getPinCode()).isEqualTo(DEFAULT_PIN_CODE);
        assertThat(testPin.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPin.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPin.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllPins() throws Exception {
        // Initialize the database
        pinRepository.saveAndFlush(pin);

        // Get all the pins
        restPinMockMvc.perform(get("/api/pins?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pin.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].pinCode").value(hasItem(DEFAULT_PIN_CODE.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getPin() throws Exception {
        // Initialize the database
        pinRepository.saveAndFlush(pin);

        // Get the pin
        restPinMockMvc.perform(get("/api/pins/{id}", pin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pin.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.pinCode").value(DEFAULT_PIN_CODE.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPin() throws Exception {
        // Get the pin
        restPinMockMvc.perform(get("/api/pins/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePin() throws Exception {
        // Initialize the database
        pinRepository.saveAndFlush(pin);
        int databaseSizeBeforeUpdate = pinRepository.findAll().size();

        // Update the pin
        Pin updatedPin = pinRepository.findOne(pin.getId());
        updatedPin.setUid(UPDATED_UID);
        updatedPin.setPinCode(UPDATED_PIN_CODE);
        updatedPin.setCreatedAt(UPDATED_CREATED_AT);
        updatedPin.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedPin.setStatus(UPDATED_STATUS);
        PinDTO pinDTO = pinMapper.pinToPinDTO(updatedPin);

        restPinMockMvc.perform(put("/api/pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pinDTO)))
                .andExpect(status().isOk());

        // Validate the Pin in the database
        List<Pin> pins = pinRepository.findAll();
        assertThat(pins).hasSize(databaseSizeBeforeUpdate);
        Pin testPin = pins.get(pins.size() - 1);
        assertThat(testPin.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testPin.getPinCode()).isEqualTo(UPDATED_PIN_CODE);
        assertThat(testPin.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPin.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPin.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deletePin() throws Exception {
        // Initialize the database
        pinRepository.saveAndFlush(pin);
        int databaseSizeBeforeDelete = pinRepository.findAll().size();

        // Get the pin
        restPinMockMvc.perform(delete("/api/pins/{id}", pin.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Pin> pins = pinRepository.findAll();
        assertThat(pins).hasSize(databaseSizeBeforeDelete - 1);
    }
}
