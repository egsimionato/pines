package qb9.web.rest;

import qb9.GiftpinApp;
import qb9.domain.CustomPin;
import qb9.repository.CustomPinRepository;
import qb9.service.CustomPinService;
import qb9.service.dto.CustomPinDTO;
import qb9.service.mapper.CustomPinMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.PinStatus;
/**
 * Test class for the CustomPinResource REST controller.
 *
 * @see CustomPinResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GiftpinApp.class)
public class CustomPinResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_PIN_CODE = "AAAAA";
    private static final String UPDATED_PIN_CODE = "BBBBB";

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final PinStatus DEFAULT_STATUS = PinStatus.NEW;
    private static final PinStatus UPDATED_STATUS = PinStatus.BURNED;

    @Inject
    private CustomPinRepository customPinRepository;

    @Inject
    private CustomPinMapper customPinMapper;

    @Inject
    private CustomPinService customPinService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restCustomPinMockMvc;

    private CustomPin customPin;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CustomPinResource customPinResource = new CustomPinResource();
        ReflectionTestUtils.setField(customPinResource, "customPinService", customPinService);
        this.restCustomPinMockMvc = MockMvcBuilders.standaloneSetup(customPinResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomPin createEntity(EntityManager em) {
        CustomPin customPin = new CustomPin();
        customPin = new CustomPin();
        customPin.setUid(DEFAULT_UID);
        customPin.setPinCode(DEFAULT_PIN_CODE);
        customPin.setCreatedAt(DEFAULT_CREATED_AT);
        customPin.setUpdatedAt(DEFAULT_UPDATED_AT);
        customPin.setStatus(DEFAULT_STATUS);
        return customPin;
    }

    @Before
    public void initTest() {
        customPin = createEntity(em);
    }

    @Test
    @Transactional
    public void createCustomPin() throws Exception {
        int databaseSizeBeforeCreate = customPinRepository.findAll().size();

        // Create the CustomPin
        CustomPinDTO customPinDTO = customPinMapper.customPinToCustomPinDTO(customPin);

        restCustomPinMockMvc.perform(post("/api/custom-pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customPinDTO)))
                .andExpect(status().isCreated());

        // Validate the CustomPin in the database
        List<CustomPin> customPins = customPinRepository.findAll();
        assertThat(customPins).hasSize(databaseSizeBeforeCreate + 1);
        CustomPin testCustomPin = customPins.get(customPins.size() - 1);
        assertThat(testCustomPin.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testCustomPin.getPinCode()).isEqualTo(DEFAULT_PIN_CODE);
        assertThat(testCustomPin.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testCustomPin.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testCustomPin.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllCustomPins() throws Exception {
        // Initialize the database
        customPinRepository.saveAndFlush(customPin);

        // Get all the customPins
        restCustomPinMockMvc.perform(get("/api/custom-pins?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(customPin.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].pinCode").value(hasItem(DEFAULT_PIN_CODE.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getCustomPin() throws Exception {
        // Initialize the database
        customPinRepository.saveAndFlush(customPin);

        // Get the customPin
        restCustomPinMockMvc.perform(get("/api/custom-pins/{id}", customPin.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customPin.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.pinCode").value(DEFAULT_PIN_CODE.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCustomPin() throws Exception {
        // Get the customPin
        restCustomPinMockMvc.perform(get("/api/custom-pins/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCustomPin() throws Exception {
        // Initialize the database
        customPinRepository.saveAndFlush(customPin);
        int databaseSizeBeforeUpdate = customPinRepository.findAll().size();

        // Update the customPin
        CustomPin updatedCustomPin = customPinRepository.findOne(customPin.getId());
        updatedCustomPin.setUid(UPDATED_UID);
        updatedCustomPin.setPinCode(UPDATED_PIN_CODE);
        updatedCustomPin.setCreatedAt(UPDATED_CREATED_AT);
        updatedCustomPin.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedCustomPin.setStatus(UPDATED_STATUS);
        CustomPinDTO customPinDTO = customPinMapper.customPinToCustomPinDTO(updatedCustomPin);

        restCustomPinMockMvc.perform(put("/api/custom-pins")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(customPinDTO)))
                .andExpect(status().isOk());

        // Validate the CustomPin in the database
        List<CustomPin> customPins = customPinRepository.findAll();
        assertThat(customPins).hasSize(databaseSizeBeforeUpdate);
        CustomPin testCustomPin = customPins.get(customPins.size() - 1);
        assertThat(testCustomPin.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testCustomPin.getPinCode()).isEqualTo(UPDATED_PIN_CODE);
        assertThat(testCustomPin.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testCustomPin.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testCustomPin.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteCustomPin() throws Exception {
        // Initialize the database
        customPinRepository.saveAndFlush(customPin);
        int databaseSizeBeforeDelete = customPinRepository.findAll().size();

        // Get the customPin
        restCustomPinMockMvc.perform(delete("/api/custom-pins/{id}", customPin.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<CustomPin> customPins = customPinRepository.findAll();
        assertThat(customPins).hasSize(databaseSizeBeforeDelete - 1);
    }
}
