package qb9.web.rest;

import qb9.GiftpinApp;
import qb9.domain.PinBatch;
import qb9.repository.PinBatchRepository;
import qb9.service.PinBatchService;
import qb9.service.dto.PinBatchDTO;
import qb9.service.mapper.PinBatchMapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import qb9.domain.enumeration.PinBatchType;
import qb9.domain.enumeration.PinBatchStatus;
/**
 * Test class for the PinBatchResource REST controller.
 *
 * @see PinBatchResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GiftpinApp.class)
public class PinBatchResourceIntTest {
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));
    private static final String DEFAULT_UID = "AAAAA";
    private static final String UPDATED_UID = "BBBBB";
    private static final String DEFAULT_CODE = "AAAAA";
    private static final String UPDATED_CODE = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final PinBatchType DEFAULT_TYPE = PinBatchType.CUSTOM;
    private static final PinBatchType UPDATED_TYPE = PinBatchType.NORMAL;

    private static final ZonedDateTime DEFAULT_CREATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_CREATED_AT_STR = dateTimeFormatter.format(DEFAULT_CREATED_AT);

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_UPDATED_AT_STR = dateTimeFormatter.format(DEFAULT_UPDATED_AT);

    private static final ZonedDateTime DEFAULT_EXPIRATION_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_EXPIRATION_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_EXPIRATION_AT_STR = dateTimeFormatter.format(DEFAULT_EXPIRATION_AT);

    private static final PinBatchStatus DEFAULT_STATUS = PinBatchStatus.NEW;
    private static final PinBatchStatus UPDATED_STATUS = PinBatchStatus.ACTIVE;

    @Inject
    private PinBatchRepository pinBatchRepository;

    @Inject
    private PinBatchMapper pinBatchMapper;

    @Inject
    private PinBatchService pinBatchService;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Inject
    private EntityManager em;

    private MockMvc restPinBatchMockMvc;

    private PinBatch pinBatch;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PinBatchResource pinBatchResource = new PinBatchResource();
        ReflectionTestUtils.setField(pinBatchResource, "pinBatchService", pinBatchService);
        this.restPinBatchMockMvc = MockMvcBuilders.standaloneSetup(pinBatchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PinBatch createEntity(EntityManager em) {
        PinBatch pinBatch = new PinBatch();
        pinBatch = new PinBatch();
        pinBatch.setUid(DEFAULT_UID);
        pinBatch.setCode(DEFAULT_CODE);
        pinBatch.setDescription(DEFAULT_DESCRIPTION);
        pinBatch.setType(DEFAULT_TYPE);
        pinBatch.setCreatedAt(DEFAULT_CREATED_AT);
        pinBatch.setUpdatedAt(DEFAULT_UPDATED_AT);
        pinBatch.setExpirationAt(DEFAULT_EXPIRATION_AT);
        pinBatch.setStatus(DEFAULT_STATUS);
        return pinBatch;
    }

    @Before
    public void initTest() {
        pinBatch = createEntity(em);
    }

    @Test
    @Transactional
    public void createPinBatch() throws Exception {
        int databaseSizeBeforeCreate = pinBatchRepository.findAll().size();

        // Create the PinBatch
        PinBatchDTO pinBatchDTO = pinBatchMapper.pinBatchToPinBatchDTO(pinBatch);

        restPinBatchMockMvc.perform(post("/api/pin-batches")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pinBatchDTO)))
                .andExpect(status().isCreated());

        // Validate the PinBatch in the database
        List<PinBatch> pinBatches = pinBatchRepository.findAll();
        assertThat(pinBatches).hasSize(databaseSizeBeforeCreate + 1);
        PinBatch testPinBatch = pinBatches.get(pinBatches.size() - 1);
        assertThat(testPinBatch.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testPinBatch.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPinBatch.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPinBatch.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testPinBatch.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPinBatch.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testPinBatch.getExpirationAt()).isEqualTo(DEFAULT_EXPIRATION_AT);
        assertThat(testPinBatch.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllPinBatches() throws Exception {
        // Initialize the database
        pinBatchRepository.saveAndFlush(pinBatch);

        // Get all the pinBatches
        restPinBatchMockMvc.perform(get("/api/pin-batches?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(pinBatch.getId().intValue())))
                .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
                .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT_STR)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT_STR)))
                .andExpect(jsonPath("$.[*].expirationAt").value(hasItem(DEFAULT_EXPIRATION_AT_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getPinBatch() throws Exception {
        // Initialize the database
        pinBatchRepository.saveAndFlush(pinBatch);

        // Get the pinBatch
        restPinBatchMockMvc.perform(get("/api/pin-batches/{id}", pinBatch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pinBatch.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT_STR))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT_STR))
            .andExpect(jsonPath("$.expirationAt").value(DEFAULT_EXPIRATION_AT_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPinBatch() throws Exception {
        // Get the pinBatch
        restPinBatchMockMvc.perform(get("/api/pin-batches/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePinBatch() throws Exception {
        // Initialize the database
        pinBatchRepository.saveAndFlush(pinBatch);
        int databaseSizeBeforeUpdate = pinBatchRepository.findAll().size();

        // Update the pinBatch
        PinBatch updatedPinBatch = pinBatchRepository.findOne(pinBatch.getId());
        updatedPinBatch.setUid(UPDATED_UID);
        updatedPinBatch.setCode(UPDATED_CODE);
        updatedPinBatch.setDescription(UPDATED_DESCRIPTION);
        updatedPinBatch.setType(UPDATED_TYPE);
        updatedPinBatch.setCreatedAt(UPDATED_CREATED_AT);
        updatedPinBatch.setUpdatedAt(UPDATED_UPDATED_AT);
        updatedPinBatch.setExpirationAt(UPDATED_EXPIRATION_AT);
        updatedPinBatch.setStatus(UPDATED_STATUS);
        PinBatchDTO pinBatchDTO = pinBatchMapper.pinBatchToPinBatchDTO(updatedPinBatch);

        restPinBatchMockMvc.perform(put("/api/pin-batches")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(pinBatchDTO)))
                .andExpect(status().isOk());

        // Validate the PinBatch in the database
        List<PinBatch> pinBatches = pinBatchRepository.findAll();
        assertThat(pinBatches).hasSize(databaseSizeBeforeUpdate);
        PinBatch testPinBatch = pinBatches.get(pinBatches.size() - 1);
        assertThat(testPinBatch.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testPinBatch.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPinBatch.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPinBatch.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPinBatch.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPinBatch.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testPinBatch.getExpirationAt()).isEqualTo(UPDATED_EXPIRATION_AT);
        assertThat(testPinBatch.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deletePinBatch() throws Exception {
        // Initialize the database
        pinBatchRepository.saveAndFlush(pinBatch);
        int databaseSizeBeforeDelete = pinBatchRepository.findAll().size();

        // Get the pinBatch
        restPinBatchMockMvc.perform(delete("/api/pin-batches/{id}", pinBatch.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PinBatch> pinBatches = pinBatchRepository.findAll();
        assertThat(pinBatches).hasSize(databaseSizeBeforeDelete - 1);
    }
}
