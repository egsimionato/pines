package qb9.repository;

import qb9.domain.Pin;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Pin entity.
 */
@SuppressWarnings("unused")
public interface PinRepository extends JpaRepository<Pin,Long> {

}
