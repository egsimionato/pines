package qb9.repository;

import qb9.domain.PinBatch;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PinBatch entity.
 */
@SuppressWarnings("unused")
public interface PinBatchRepository extends JpaRepository<PinBatch,Long> {

}
