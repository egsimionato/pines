package qb9.repository;

import qb9.domain.CustomPin;

import org.springframework.data.jpa.repository.*;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the CustomPin entity.
 */
@SuppressWarnings("unused")
public interface CustomPinRepository extends JpaRepository<CustomPin,Long> {

    Optional<CustomPin> findOneByPinCode(String pinCode);

}
