package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import qb9.domain.enumeration.PinBatchType;

import qb9.domain.enumeration.PinBatchStatus;

import java.util.UUID;

/**
 * A PinBatch.
 */
@Entity
@Table(name = "tbl_batch")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PinBatch implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final int lifespanInMonth = 6;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private PinBatchType type;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Column(name = "expiration_at")
    private ZonedDateTime expirationAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PinBatchStatus status;

    public static PinBatch CustomPinBatch() {
        PinBatch o = new PinBatch();
        o.uid = "pin.pin." + UUID.randomUUID();
        o.createdAt = ZonedDateTime.now();
        o.updatedAt = o.createdAt;
        o.type = PinBatchType.CUSTOM;
        o.status = PinBatchStatus.NEW;
        o.expirationAt = o.createdAt.plusMonths(lifespanInMonth);
        return o;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public PinBatch setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getCode() {
        return code;
    }

    public PinBatch setCode(String code) {
        this.code = code;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public PinBatch setDescription(String description) {
        this.description = description;
        return this;
    }

    public PinBatchType getType() {
        return type;
    }

    public PinBatch setType(PinBatchType type) {
        this.type = type;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public PinBatch setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public PinBatch setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public ZonedDateTime getExpirationAt() {
        return expirationAt;
    }

    public PinBatch setExpirationAt(ZonedDateTime expirationAt) {
        this.expirationAt = expirationAt;
        return this;
    }

    public PinBatchStatus getStatus() {
        return status;
    }

    public PinBatch setStatus(PinBatchStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PinBatch pinBatch = (PinBatch) o;
        if(pinBatch.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pinBatch.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PinBatch{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", type='" + type + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", expirationAt='" + expirationAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
