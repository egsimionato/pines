package qb9.domain.enumeration;

/**
 * The PinStatus enumeration.
 */
public enum PinStatus {
    NEW,BURNED,CANCEL
}
