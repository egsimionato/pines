package qb9.domain.enumeration;

/**
 * The PinBatchType enumeration.
 */
public enum PinBatchType {
    CUSTOM,NORMAL
}
