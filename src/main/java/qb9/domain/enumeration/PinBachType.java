package qb9.domain.enumeration;

/**
 * The PinBachType enumeration.
 */
public enum PinBachType {
    CUSTOM,NORMAL
}
