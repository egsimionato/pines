package qb9.domain.enumeration;

/**
 * The PinBachStatus enumeration.
 */
public enum PinBachStatus {
    NEW,ACTIVE,PAUSE,CANCEL,COMPLETED
}
