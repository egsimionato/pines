package qb9.domain.enumeration;

/**
 * The PinBatchStatus enumeration.
 */
public enum PinBatchStatus {
    NEW,ACTIVE,PAUSE,CANCEL,COMPLETED
}
