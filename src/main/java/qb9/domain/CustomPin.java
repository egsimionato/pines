package qb9.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import java.util.UUID;

import java.lang.Exception;

import qb9.domain.enumeration.PinStatus;

/**
 * A CustomPin.
 */
@Entity
@Table(name = "tbl_custom_pin")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CustomPin implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uid")
    private String uid;

    @Column(name = "pin_code")
    private String pinCode;

    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PinStatus status;

    @ManyToOne
    private PinBatch batch;

    public static CustomPin New(PinBatch batch) {
       CustomPin o = new CustomPin();
       UUID rand = UUID.randomUUID();
       o.uid = "pin.cpin." + rand;
       o.pinCode = rand.toString().substring(
               rand.toString().lastIndexOf("-")+1);
       o.createdAt = ZonedDateTime.now();
       o.updatedAt = o.createdAt;
       o.status = PinStatus.NEW;
       o.batch = batch;
       return o;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public CustomPin setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getPinCode() {
        return pinCode;
    }

    public CustomPin setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public CustomPin setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public CustomPin setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public PinStatus getStatus() {
        return status;
    }

    public CustomPin setStatus(PinStatus status) {
        this.status = status;
        return this;
    }

    public PinBatch getBatch() {
        return batch;
    }

    public CustomPin setBatch(PinBatch pinBatch) {
        this.batch = pinBatch;
        return this;
    }

    public Boolean canBurn() {
        //return status == PinStatus.NEW;
        return true;
    }

    public CustomPin burn() {
        if (canBurn()) {
            status = PinStatus.BURNED;
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CustomPin customPin = (CustomPin) o;
        if(customPin.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, customPin.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CustomPin{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", pinCode='" + pinCode + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
