package qb9.web.rest;

import qb9.web.rest.errors.ErrorVM;
import qb9.web.rest.errors.ErrorConstants;

import qb9.domain.CustomPin;
import qb9.service.BurnPinService;
import qb9.repository.CustomPinRepository;
import qb9.service.dto.BurnDTO;
import qb9.service.dto.BurnStatus;

import java.lang.Math;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import io.swagger.annotations.*;
import com.codahale.metrics.annotation.Timed;

@Component
@RestController
@Transactional
public class BurnPinController {

    private final Logger log = LoggerFactory.getLogger(BurnPinController.class);

    @Inject
    private CustomPinRepository customPinRepository;

    @Inject
    private BurnPinService burnerService;

    /**
     * POST  burn/{pinCode} : Unlock a random
     *
     * @param UnlockComicDTO the unlock to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the username o comic are not registred,
     * or status 500 (Internal Server Error) if the unlock couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Burn a PinCode",
            nickname = "burn_pincode",
            notes = "Burn a PinCode")
    @RequestMapping(value = "/api/burn/{pinCode}",
            method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = BurnDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> burnPin(
            @ApiParam(value="pinCode", required=true) @PathVariable String pinCode,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to burn {} pinCode", pinCode);

        CustomPin customPin = null;
        Optional<CustomPin> oCustomPin = customPinRepository.findOneByPinCode(pinCode);
        if (!oCustomPin.isPresent()) {
            log.debug("@Z The {} PinCode no exists", pinCode);
             ErrorVM errorDTO = new ErrorVM(
                  "PIN_NO_EXISTS", "PinCode no exists");
            return new ResponseEntity<>(false, HttpStatus.OK);
        } else {
            customPin = oCustomPin.get();
        }

        BurnDTO burnDTO = null;
        Optional<BurnDTO> oBurnDTO = burnerService.burn(customPin);
        log.debug("@Z oDTO {}", oBurnDTO);

        if (!oBurnDTO.isPresent()) {
            ErrorVM errorDTO = new ErrorVM(
                     ErrorConstants.ERR_VALIDATION, ErrorConstants.ERR_VALIDATION);
            return new ResponseEntity<>(false, HttpStatus.OK);
        }
        burnDTO = oBurnDTO.get();
        log.debug("@Z DTO {}", burnDTO);

        if (burnDTO.hasError()) {
            ErrorVM errorDTO = new ErrorVM(burnDTO.getStatus().toString(), burnDTO.getStatus().toString());
            return new ResponseEntity<>(false, HttpStatus.OK);
        }

        return new ResponseEntity<>(burnDTO.isSuccess(), HttpStatus.OK);
    }

    /**
     * POST  canburn/{pinCode} : Unlock a random
     *
     * @param DTO the unlock to processed.
     * @return the ResponseEntity with status 200 (Created) and with body the AccountInfoDTO information,
     * or with status 400 (Bad Request) if the username o comic are not registred,
     * or status 500 (Internal Server Error) if the unlock couldn't be done.
     *
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Can burn a PinCode",
            nickname = "canburn_pincode",
            notes = "Can Burn a PinCode")
    @RequestMapping(value = "/api/canburn/{pinCode}",
            method = RequestMethod.POST)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = BurnDTO.class),
        @ApiResponse(code = 400, message = "Invalid params"),
        @ApiResponse(code = 401, message = "Unauthorized"),
        @ApiResponse(code = 403, message = "Forbidden"),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")})
    @Timed
    public ResponseEntity<?> canBurnPin(
            @ApiParam(value="pinCode", required=true) @PathVariable String pinCode,
            @ApiParam(value="enable debug.", required=false) @RequestParam(value="debug", required=false) Optional<Boolean> isDebugEnabled,
            HttpServletResponse response) {
        log.debug("REST request to get if {} pinCode can burn", pinCode);

        CustomPin customPin = null;
        Optional<CustomPin> oCustomPin = customPinRepository.findOneByPinCode(pinCode);
        if (!oCustomPin.isPresent()) {
            log.debug("@Z The {} PinCode no exists", pinCode);
             ErrorVM errorDTO = new ErrorVM("PIN_NO_EXISTS", "PinCode no exists");
            return new ResponseEntity<>(false, HttpStatus.OK);
        } else {
            customPin = oCustomPin.get();
        }

        return new ResponseEntity<>(customPin.canBurn(), HttpStatus.OK);
    }


}
