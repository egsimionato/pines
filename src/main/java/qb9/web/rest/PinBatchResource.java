package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.PinBatchService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.PinBatchDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing PinBatch.
 */
@RestController
@RequestMapping("/api")
public class PinBatchResource {

    private final Logger log = LoggerFactory.getLogger(PinBatchResource.class);
        
    @Inject
    private PinBatchService pinBatchService;

    /**
     * POST  /pin-batches : Create a new pinBatch.
     *
     * @param pinBatchDTO the pinBatchDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pinBatchDTO, or with status 400 (Bad Request) if the pinBatch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/pin-batches",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinBatchDTO> createPinBatch(@RequestBody PinBatchDTO pinBatchDTO) throws URISyntaxException {
        log.debug("REST request to save PinBatch : {}", pinBatchDTO);
        if (pinBatchDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("pinBatch", "idexists", "A new pinBatch cannot already have an ID")).body(null);
        }
        PinBatchDTO result = pinBatchService.save(pinBatchDTO);
        return ResponseEntity.created(new URI("/api/pin-batches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pinBatch", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pin-batches : Updates an existing pinBatch.
     *
     * @param pinBatchDTO the pinBatchDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pinBatchDTO,
     * or with status 400 (Bad Request) if the pinBatchDTO is not valid,
     * or with status 500 (Internal Server Error) if the pinBatchDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/pin-batches",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinBatchDTO> updatePinBatch(@RequestBody PinBatchDTO pinBatchDTO) throws URISyntaxException {
        log.debug("REST request to update PinBatch : {}", pinBatchDTO);
        if (pinBatchDTO.getId() == null) {
            return createPinBatch(pinBatchDTO);
        }
        PinBatchDTO result = pinBatchService.save(pinBatchDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pinBatch", pinBatchDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pin-batches : get all the pinBatches.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pinBatches in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/pin-batches",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PinBatchDTO>> getAllPinBatches(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PinBatches");
        Page<PinBatchDTO> page = pinBatchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pin-batches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pin-batches/:id : get the "id" pinBatch.
     *
     * @param id the id of the pinBatchDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pinBatchDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/pin-batches/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinBatchDTO> getPinBatch(@PathVariable Long id) {
        log.debug("REST request to get PinBatch : {}", id);
        PinBatchDTO pinBatchDTO = pinBatchService.findOne(id);
        return Optional.ofNullable(pinBatchDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pin-batches/:id : delete the "id" pinBatch.
     *
     * @param id the id of the pinBatchDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/pin-batches/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePinBatch(@PathVariable Long id) {
        log.debug("REST request to delete PinBatch : {}", id);
        pinBatchService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pinBatch", id.toString())).build();
    }

}
