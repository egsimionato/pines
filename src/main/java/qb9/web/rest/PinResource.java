package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.PinService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.PinDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Pin.
 */
@RestController
@RequestMapping("/api")
public class PinResource {

    private final Logger log = LoggerFactory.getLogger(PinResource.class);
        
    @Inject
    private PinService pinService;

    /**
     * POST  /pins : Create a new pin.
     *
     * @param pinDTO the pinDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pinDTO, or with status 400 (Bad Request) if the pin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/pins",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinDTO> createPin(@RequestBody PinDTO pinDTO) throws URISyntaxException {
        log.debug("REST request to save Pin : {}", pinDTO);
        if (pinDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("pin", "idexists", "A new pin cannot already have an ID")).body(null);
        }
        PinDTO result = pinService.save(pinDTO);
        return ResponseEntity.created(new URI("/api/pins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("pin", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pins : Updates an existing pin.
     *
     * @param pinDTO the pinDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pinDTO,
     * or with status 400 (Bad Request) if the pinDTO is not valid,
     * or with status 500 (Internal Server Error) if the pinDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/pins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinDTO> updatePin(@RequestBody PinDTO pinDTO) throws URISyntaxException {
        log.debug("REST request to update Pin : {}", pinDTO);
        if (pinDTO.getId() == null) {
            return createPin(pinDTO);
        }
        PinDTO result = pinService.save(pinDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("pin", pinDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pins : get all the pins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/pins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PinDTO>> getAllPins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Pins");
        Page<PinDTO> page = pinService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pins/:id : get the "id" pin.
     *
     * @param id the id of the pinDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pinDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/pins/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PinDTO> getPin(@PathVariable Long id) {
        log.debug("REST request to get Pin : {}", id);
        PinDTO pinDTO = pinService.findOne(id);
        return Optional.ofNullable(pinDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pins/:id : delete the "id" pin.
     *
     * @param id the id of the pinDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/pins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deletePin(@PathVariable Long id) {
        log.debug("REST request to delete Pin : {}", id);
        pinService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("pin", id.toString())).build();
    }

}
