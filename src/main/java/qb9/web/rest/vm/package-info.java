/**
 * View Models used by Spring MVC REST controllers.
 */
package qb9.web.rest.vm;
