package qb9.web.rest;

import com.codahale.metrics.annotation.Timed;
import qb9.service.CustomPinService;
import qb9.web.rest.util.HeaderUtil;
import qb9.web.rest.util.PaginationUtil;
import qb9.service.dto.CustomPinDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing CustomPin.
 */
@RestController
@RequestMapping("/api")
public class CustomPinResource {

    private final Logger log = LoggerFactory.getLogger(CustomPinResource.class);
        
    @Inject
    private CustomPinService customPinService;

    /**
     * POST  /custom-pins : Create a new customPin.
     *
     * @param customPinDTO the customPinDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customPinDTO, or with status 400 (Bad Request) if the customPin has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-pins",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomPinDTO> createCustomPin(@RequestBody CustomPinDTO customPinDTO) throws URISyntaxException {
        log.debug("REST request to save CustomPin : {}", customPinDTO);
        if (customPinDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("customPin", "idexists", "A new customPin cannot already have an ID")).body(null);
        }
        CustomPinDTO result = customPinService.save(customPinDTO);
        return ResponseEntity.created(new URI("/api/custom-pins/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("customPin", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /custom-pins : Updates an existing customPin.
     *
     * @param customPinDTO the customPinDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customPinDTO,
     * or with status 400 (Bad Request) if the customPinDTO is not valid,
     * or with status 500 (Internal Server Error) if the customPinDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/custom-pins",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomPinDTO> updateCustomPin(@RequestBody CustomPinDTO customPinDTO) throws URISyntaxException {
        log.debug("REST request to update CustomPin : {}", customPinDTO);
        if (customPinDTO.getId() == null) {
            return createCustomPin(customPinDTO);
        }
        CustomPinDTO result = customPinService.save(customPinDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("customPin", customPinDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /custom-pins : get all the customPins.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of customPins in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/custom-pins",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<CustomPinDTO>> getAllCustomPins(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of CustomPins");
        Page<CustomPinDTO> page = customPinService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/custom-pins");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /custom-pins/:id : get the "id" customPin.
     *
     * @param id the id of the customPinDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customPinDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/custom-pins/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<CustomPinDTO> getCustomPin(@PathVariable Long id) {
        log.debug("REST request to get CustomPin : {}", id);
        CustomPinDTO customPinDTO = customPinService.findOne(id);
        return Optional.ofNullable(customPinDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /custom-pins/:id : delete the "id" customPin.
     *
     * @param id the id of the customPinDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/custom-pins/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteCustomPin(@PathVariable Long id) {
        log.debug("REST request to delete CustomPin : {}", id);
        customPinService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("customPin", id.toString())).build();
    }

}
