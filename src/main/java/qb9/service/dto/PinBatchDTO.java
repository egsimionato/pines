package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Objects;

import qb9.domain.enumeration.PinBatchType;
import qb9.domain.enumeration.PinBatchStatus;

/**
 * A DTO for the PinBatch entity.
 */
public class PinBatchDTO implements Serializable {

    private Long id;

    private String uid;

    private String code;

    private String description;

    private PinBatchType type;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private ZonedDateTime expirationAt;

    private PinBatchStatus status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public PinBatchDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getCode() {
        return code;
    }

    public PinBatchDTO setCode(String code) {
        this.code = code;
        return this;
    }
    public String getDescription() {
        return description;
    }

    public PinBatchDTO setDescription(String description) {
        this.description = description;
        return this;
    }
    public PinBatchType getType() {
        return type;
    }

    public PinBatchDTO setType(PinBatchType type) {
        this.type = type;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public PinBatchDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public PinBatchDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public ZonedDateTime getExpirationAt() {
        return expirationAt;
    }

    public PinBatchDTO setExpirationAt(ZonedDateTime expirationAt) {
        this.expirationAt = expirationAt;
        return this;
    }
    public PinBatchStatus getStatus() {
        return status;
    }

    public PinBatchDTO setStatus(PinBatchStatus status) {
        this.status = status;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PinBatchDTO pinBatchDTO = (PinBatchDTO) o;

        if ( ! Objects.equals(id, pinBatchDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PinBatchDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", code='" + code + "'" +
            ", description='" + description + "'" +
            ", type='" + type + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", expirationAt='" + expirationAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
