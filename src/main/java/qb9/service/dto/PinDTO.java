package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.PinStatus;

/**
 * A DTO for the Pin entity.
 */
public class PinDTO implements Serializable {

    private Long id;

    private String uid;

    private String pinCode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private PinStatus status;


    private Long batchId;
    

    private String batchCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public PinDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getPinCode() {
        return pinCode;
    }

    public PinDTO setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public PinDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public PinDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public PinStatus getStatus() {
        return status;
    }

    public PinDTO setStatus(PinStatus status) {
        this.status = status;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }

    public PinDTO setBatchId(Long pinBatchId) {
        this.batchId = pinBatchId;
        return this;
    }


    public String getBatchCode() {
        return batchCode;
    }

    public PinDTO setBatchCode(String pinBatchCode) {
        this.batchCode = pinBatchCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PinDTO pinDTO = (PinDTO) o;

        if ( ! Objects.equals(id, pinDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PinDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", pinCode='" + pinCode + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
