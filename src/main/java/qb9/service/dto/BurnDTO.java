package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.service.dto.BurnStatus;

/**
 * A DTO for the Pin entity.
 */
public class BurnDTO implements Serializable {

    private BurnStatus status;

    public static BurnDTO SUCCESS() {
        BurnDTO dto = new BurnDTO();
        dto.status = BurnStatus.SUCCESS;
        return dto;
    }

    public static BurnDTO PREVIOUSLY_BURNED() {
        BurnDTO dto = new BurnDTO();
        dto.status = BurnStatus.PREVIOUSLY_BURNED;
        return dto;
    }


    public BurnStatus getStatus() {
        return status;
    }

    public BurnDTO setStatus(BurnStatus status) {
        this.status = status;
        return this;
    }

    public Boolean isSuccess() {
        return this.status == BurnStatus.SUCCESS;
    }

    public Boolean hasError() {
        return this.status != BurnStatus.SUCCESS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BurnDTO burnDTO = (BurnDTO) o;

        if ( ! Objects.equals(status, burnDTO.status)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(status);
    }

    @Override
    public String toString() {
        return "BurnDTO{" +
            ", status='" + status + "'" +
            '}';
    }
}
