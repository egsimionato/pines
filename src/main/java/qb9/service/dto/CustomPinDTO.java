package qb9.service.dto;

import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import qb9.domain.enumeration.PinStatus;

/**
 * A DTO for the CustomPin entity.
 */
public class CustomPinDTO implements Serializable {

    private Long id;

    private String uid;

    private String pinCode;

    private ZonedDateTime createdAt;

    private ZonedDateTime updatedAt;

    private PinStatus status;


    private Long batchId;
    

    private String batchCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getUid() {
        return uid;
    }

    public CustomPinDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }
    public String getPinCode() {
        return pinCode;
    }

    public CustomPinDTO setPinCode(String pinCode) {
        this.pinCode = pinCode;
        return this;
    }
    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public CustomPinDTO setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }
    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public CustomPinDTO setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }
    public PinStatus getStatus() {
        return status;
    }

    public CustomPinDTO setStatus(PinStatus status) {
        this.status = status;
        return this;
    }

    public Long getBatchId() {
        return batchId;
    }

    public CustomPinDTO setBatchId(Long pinBatchId) {
        this.batchId = pinBatchId;
        return this;
    }


    public String getBatchCode() {
        return batchCode;
    }

    public CustomPinDTO setBatchCode(String pinBatchCode) {
        this.batchCode = pinBatchCode;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomPinDTO customPinDTO = (CustomPinDTO) o;

        if ( ! Objects.equals(id, customPinDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CustomPinDTO{" +
            "id=" + id +
            ", uid='" + uid + "'" +
            ", pinCode='" + pinCode + "'" +
            ", createdAt='" + createdAt + "'" +
            ", updatedAt='" + updatedAt + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
