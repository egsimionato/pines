package qb9.service.dto;

/**
 * The PinStatus enumeration.
 */
public enum BurnStatus {
    SUCCESS,PREVIOUSLY_BURNED
}
