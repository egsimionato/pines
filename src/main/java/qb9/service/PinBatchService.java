package qb9.service;

import qb9.service.dto.PinBatchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing PinBatch.
 */
public interface PinBatchService {

    /**
     * Save a pinBatch.
     *
     * @param pinBatchDTO the entity to save
     * @return the persisted entity
     */
    PinBatchDTO save(PinBatchDTO pinBatchDTO);

    /**
     *  Get all the pinBatches.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PinBatchDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" pinBatch.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PinBatchDTO findOne(Long id);

    /**
     *  Delete the "id" pinBatch.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
