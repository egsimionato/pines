package qb9.service;

import qb9.service.dto.CustomPinDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing CustomPin.
 */
public interface CustomPinService {

    /**
     * Save a customPin.
     *
     * @param customPinDTO the entity to save
     * @return the persisted entity
     */
    CustomPinDTO save(CustomPinDTO customPinDTO);

    /**
     *  Get all the customPins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<CustomPinDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" customPin.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    CustomPinDTO findOne(Long id);

    /**
     *  Delete the "id" customPin.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
