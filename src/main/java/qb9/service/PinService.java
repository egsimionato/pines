package qb9.service;

import qb9.service.dto.PinDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.LinkedList;
import java.util.List;

/**
 * Service Interface for managing Pin.
 */
public interface PinService {

    /**
     * Save a pin.
     *
     * @param pinDTO the entity to save
     * @return the persisted entity
     */
    PinDTO save(PinDTO pinDTO);

    /**
     *  Get all the pins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PinDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" pin.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PinDTO findOne(Long id);

    /**
     *  Delete the "id" pin.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
