package qb9.service;

import qb9.domain.CustomPin;
import qb9.service.dto.BurnDTO;

import java.util.Optional;

public interface BurnPinService {

    public Optional<BurnDTO> burn(CustomPin pin);

}
