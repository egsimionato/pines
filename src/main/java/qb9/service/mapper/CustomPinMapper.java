package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.CustomPinDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity CustomPin and its DTO CustomPinDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CustomPinMapper {

    @Mapping(source = "batch.id", target = "batchId")
    @Mapping(source = "batch.code", target = "batchCode")
    CustomPinDTO customPinToCustomPinDTO(CustomPin customPin);

    List<CustomPinDTO> customPinsToCustomPinDTOs(List<CustomPin> customPins);

    @Mapping(source = "batchId", target = "batch")
    CustomPin customPinDTOToCustomPin(CustomPinDTO customPinDTO);

    List<CustomPin> customPinDTOsToCustomPins(List<CustomPinDTO> customPinDTOs);

    default PinBatch pinBatchFromId(Long id) {
        if (id == null) {
            return null;
        }
        PinBatch pinBatch = new PinBatch();
        pinBatch.setId(id);
        return pinBatch;
    }
}
