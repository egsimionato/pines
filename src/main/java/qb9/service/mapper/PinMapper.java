package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.PinDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Pin and its DTO PinDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PinMapper {

    @Mapping(source = "batch.id", target = "batchId")
    @Mapping(source = "batch.code", target = "batchCode")
    PinDTO pinToPinDTO(Pin pin);

    List<PinDTO> pinsToPinDTOs(List<Pin> pins);

    @Mapping(source = "batchId", target = "batch")
    Pin pinDTOToPin(PinDTO pinDTO);

    List<Pin> pinDTOsToPins(List<PinDTO> pinDTOs);

    default PinBatch pinBatchFromId(Long id) {
        if (id == null) {
            return null;
        }
        PinBatch pinBatch = new PinBatch();
        pinBatch.setId(id);
        return pinBatch;
    }
}
