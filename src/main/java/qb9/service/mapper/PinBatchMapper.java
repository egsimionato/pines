package qb9.service.mapper;

import qb9.domain.*;
import qb9.service.dto.PinBatchDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity PinBatch and its DTO PinBatchDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PinBatchMapper {

    PinBatchDTO pinBatchToPinBatchDTO(PinBatch pinBatch);

    List<PinBatchDTO> pinBatchesToPinBatchDTOs(List<PinBatch> pinBatches);

    PinBatch pinBatchDTOToPinBatch(PinBatchDTO pinBatchDTO);

    List<PinBatch> pinBatchDTOsToPinBatches(List<PinBatchDTO> pinBatchDTOs);
}
