package qb9.service.impl;

import qb9.service.CustomPinService;
import qb9.domain.CustomPin;
import qb9.repository.CustomPinRepository;
import qb9.service.dto.CustomPinDTO;
import qb9.service.mapper.CustomPinMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CustomPin.
 */
@Service
@Transactional
public class CustomPinServiceImpl implements CustomPinService{

    private final Logger log = LoggerFactory.getLogger(CustomPinServiceImpl.class);
    
    @Inject
    private CustomPinRepository customPinRepository;

    @Inject
    private CustomPinMapper customPinMapper;

    /**
     * Save a customPin.
     *
     * @param customPinDTO the entity to save
     * @return the persisted entity
     */
    public CustomPinDTO save(CustomPinDTO customPinDTO) {
        log.debug("Request to save CustomPin : {}", customPinDTO);
        CustomPin customPin = customPinMapper.customPinDTOToCustomPin(customPinDTO);
        customPin = customPinRepository.save(customPin);
        CustomPinDTO result = customPinMapper.customPinToCustomPinDTO(customPin);
        return result;
    }

    /**
     *  Get all the customPins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<CustomPinDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CustomPins");
        Page<CustomPin> result = customPinRepository.findAll(pageable);
        return result.map(customPin -> customPinMapper.customPinToCustomPinDTO(customPin));
    }

    /**
     *  Get one customPin by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public CustomPinDTO findOne(Long id) {
        log.debug("Request to get CustomPin : {}", id);
        CustomPin customPin = customPinRepository.findOne(id);
        CustomPinDTO customPinDTO = customPinMapper.customPinToCustomPinDTO(customPin);
        return customPinDTO;
    }

    /**
     *  Delete the  customPin by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CustomPin : {}", id);
        customPinRepository.delete(id);
    }
}
