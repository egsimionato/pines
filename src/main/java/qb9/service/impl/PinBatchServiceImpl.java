package qb9.service.impl;

import qb9.service.PinBatchService;
import qb9.domain.PinBatch;
import qb9.repository.PinBatchRepository;
import qb9.service.dto.PinBatchDTO;
import qb9.service.mapper.PinBatchMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PinBatch.
 */
@Service
@Transactional
public class PinBatchServiceImpl implements PinBatchService{

    private final Logger log = LoggerFactory.getLogger(PinBatchServiceImpl.class);
    
    @Inject
    private PinBatchRepository pinBatchRepository;

    @Inject
    private PinBatchMapper pinBatchMapper;

    /**
     * Save a pinBatch.
     *
     * @param pinBatchDTO the entity to save
     * @return the persisted entity
     */
    public PinBatchDTO save(PinBatchDTO pinBatchDTO) {
        log.debug("Request to save PinBatch : {}", pinBatchDTO);
        PinBatch pinBatch = pinBatchMapper.pinBatchDTOToPinBatch(pinBatchDTO);
        pinBatch = pinBatchRepository.save(pinBatch);
        PinBatchDTO result = pinBatchMapper.pinBatchToPinBatchDTO(pinBatch);
        return result;
    }

    /**
     *  Get all the pinBatches.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PinBatchDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PinBatches");
        Page<PinBatch> result = pinBatchRepository.findAll(pageable);
        return result.map(pinBatch -> pinBatchMapper.pinBatchToPinBatchDTO(pinBatch));
    }

    /**
     *  Get one pinBatch by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PinBatchDTO findOne(Long id) {
        log.debug("Request to get PinBatch : {}", id);
        PinBatch pinBatch = pinBatchRepository.findOne(id);
        PinBatchDTO pinBatchDTO = pinBatchMapper.pinBatchToPinBatchDTO(pinBatch);
        return pinBatchDTO;
    }

    /**
     *  Delete the  pinBatch by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete PinBatch : {}", id);
        pinBatchRepository.delete(id);
    }
}
