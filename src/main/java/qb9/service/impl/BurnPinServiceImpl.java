package qb9.service.impl;

import qb9.domain.CustomPin;
import qb9.domain.enumeration.PinStatus;
import qb9.repository.CustomPinRepository;

import qb9.service.BurnPinService;
import qb9.service.dto.BurnStatus;
import qb9.service.dto.BurnDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import javax.inject.Inject;

@Service
@Transactional
public class BurnPinServiceImpl implements BurnPinService {

    private final Logger log = LoggerFactory.getLogger(BurnPinServiceImpl.class);

    @Inject
    CustomPinRepository customPinRepository;

    public Optional<BurnDTO> burn(CustomPin pin) {
        log.debug("Request to perform a burn a {} PinCode", pin.getPinCode());

        if (!pin.canBurn()) {
            log.debug("@Z The {} PinCode cannot burn", pin.getPinCode());
            return Optional.of(BurnDTO.PREVIOUSLY_BURNED());
        }

        pin.burn();
        customPinRepository.save(pin);

        return Optional.of(BurnDTO.SUCCESS());
    }


}
