package qb9.service.impl;

import qb9.service.PinService;
import qb9.domain.Pin;
import qb9.repository.PinRepository;
import qb9.service.dto.PinDTO;
import qb9.service.mapper.PinMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Pin.
 */
@Service
@Transactional
public class PinServiceImpl implements PinService{

    private final Logger log = LoggerFactory.getLogger(PinServiceImpl.class);
    
    @Inject
    private PinRepository pinRepository;

    @Inject
    private PinMapper pinMapper;

    /**
     * Save a pin.
     *
     * @param pinDTO the entity to save
     * @return the persisted entity
     */
    public PinDTO save(PinDTO pinDTO) {
        log.debug("Request to save Pin : {}", pinDTO);
        Pin pin = pinMapper.pinDTOToPin(pinDTO);
        pin = pinRepository.save(pin);
        PinDTO result = pinMapper.pinToPinDTO(pin);
        return result;
    }

    /**
     *  Get all the pins.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true) 
    public Page<PinDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pins");
        Page<Pin> result = pinRepository.findAll(pageable);
        return result.map(pin -> pinMapper.pinToPinDTO(pin));
    }

    /**
     *  Get one pin by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true) 
    public PinDTO findOne(Long id) {
        log.debug("Request to get Pin : {}", id);
        Pin pin = pinRepository.findOne(id);
        PinDTO pinDTO = pinMapper.pinToPinDTO(pin);
        return pinDTO;
    }

    /**
     *  Delete the  pin by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Pin : {}", id);
        pinRepository.delete(id);
    }
}
